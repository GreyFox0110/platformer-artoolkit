﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Button { none = -1, up = 1, down = 2, left = 3, right = 4, jump = 5}
[RequireComponent(typeof(CharacterController))]
public class move : MonoBehaviour {

    public float speed = 1;
    //public float jumpSpeed = 8.0F;

    //private bool canJump = false;
    //private bool isFalling = false;
    //private Button _buttonName;

    //private float forwardValue = 0;
    //private float horizontalValue = 0;
    //private float upValue = 0;
    //private Vector3 initPosition;


    public float verticalVelocity;
    public float jumpForce = 1.0f;
    public float gravityJump = 14.0f;

    //private float currentTimer = 0;
    //private float maxTimer = 2f;

    ////public float speed = 3.0F;
    //public float rotateSpeed = 3.0F;
    public float gravity = 20.0F;
    public Material platformMat;
    private Color originalColor;
    //private Vector3 moveDirection = Vector3.zero;

    // Use this for initialization
    void Start () {
        // _buttonName = Button.none;
        //initPosition = transform.position;
        originalColor = platformMat.color;
    }
	
	// Update is called once per frame
	void Update () {

        CharacterController controller = GetComponent<CharacterController>();

        float deltaX = Input.GetAxis("Horizontal") * speed;
        float deltaZ = Input.GetAxis("Vertical") * speed;
        Vector3 movement = new Vector3(deltaX, 0, deltaZ);
        movement = Vector3.ClampMagnitude(movement, speed);

        movement.y = gravity;

        movement *= Time.deltaTime;
        movement = transform.TransformDirection(movement);
        controller.Move(movement);


        if (controller.isGrounded)
        {
            verticalVelocity = -gravityJump * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                verticalVelocity = jumpForce;
            }
        }
        else
        {
            verticalVelocity -= gravityJump * Time.deltaTime;
        }

        Vector3 jumpVector = new Vector3(0, verticalVelocity, 0);
        controller.Move(jumpVector * Time.deltaTime);


        //transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
        //Vector3 forward = transform.TransformDirection(Vector3.forward);
        //float curSpeed = speed * Input.GetAxis("Vertical");
        //controller.SimpleMove(forward * curSpeed);

        //if (controller.isGrounded && Input.GetButton("Jump"))
        //{
        //    moveDirection.y = jumpSpeed;
        //}

        //moveDirection.y -= gravity * Time.deltaTime;
        //controller.Move(moveDirection * Time.deltaTime);
        //if (isFalling)
        //    return;

        //CharacterController controller = GetComponent<CharacterController>();
        //Rigidbody body = GetComponent<Rigidbody>();

        //if (canJump)
        //{
        //    upValue += Time.deltaTime * jumpSpeed;

        //    if (transform.position.y >= .05f)
        //    {
        //        canJump = false;
        //        upValue = 0;
        //    }
        //}
        //else if (!canJump)
        //{
        //    upValue -= Time.deltaTime * jumpSpeed;

        //    if (transform.position.y <= 0.008f && !isFalling)
        //        upValue = 0;
        //}

        //switch ((int)_buttonName)
        //{
        //    case -1:
        //        {
        //            forwardValue = 0;
        //            horizontalValue = 0;
        //        }
        //        break;
        //    case 1:
        //        {
        //            forwardValue += Time.deltaTime * speed;

        //            if (forwardValue >= 1)
        //                forwardValue = 1;
        //        }
        //        break;
        //    case 2:
        //        {
        //            forwardValue -= Time.deltaTime * speed;
        //            if (forwardValue <= -1)
        //                forwardValue = -1;
        //        }
        //        break;
        //    case 3:
        //        {
        //            horizontalValue -= Time.deltaTime * speed;
        //            if (horizontalValue <= -1)
        //                horizontalValue = -1;
        //        }
        //        break;
        //    case 4:
        //        {
        //            horizontalValue += Time.deltaTime * speed;

        //            if (forwardValue >= 1)
        //                horizontalValue = 1;
        //        }
        //        break;

        //}
        //transform.Translate(horizontalValue, upValue, forwardValue);
    }

    /*public void OnButtonPressed(GameObject button)
    {
        switch (button.name)
        {
            case "left":
                {
                    _buttonName = Button.left;
                }
                break;
            case "right":
                {
                    _buttonName = Button.right;
                }
                break;
            case "up":
                {
                    _buttonName = Button.up;
                }
                break;
            case "down":
                {
                    _buttonName = Button.down;
                }
                break;
            case "jump":
                {
                    _buttonName = Button.jump;
                }
                break;
        }
    }

    public void OnButtonReleased ()
    {
        _buttonName = Button.none;
    }

    public void OnButtonUp()
    {
        if (!canJump)
            canJump = true;
    }

    private void FixedUpdate()
    {
        //RaycastHit hit;

        //if (!canJump)
        //{
        //    if (Physics.Raycast(new Ray(transform.position, Vector3.down), out hit))
        //    {
        //        if (hit.collider.tag == "Floor")
        //            currentTimer = 0;
        //        else
        //            Debug.Log("nf");
        //    }
        //    currentTimer += Time.deltaTime;
        //    if (currentTimer > maxTimer)
        //    {
        //        GetComponent<Rigidbody>().useGravity = true;
        //        isFalling = true;
        //    }
        //}
    }*/

    public void StartGame()
    {
        platformMat.color = new Color(1, 1, 1, 0);
    }

    private void OnDestroy()
    {
        platformMat.color = originalColor;
    }
}
